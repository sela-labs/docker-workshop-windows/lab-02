# Docker Workshop - Windows
Lab 02: Building more complex images

---

## Preparations

 - Create a new folder for the lab:
```
$ mkdir lab-02
$ cd lab-02
```

## Instructions

 - Create a "index.html" file with the following content:
```
<html>
    <head>
        <style>
        body {
        	background-image: linear-gradient(-74deg, transparent 90%, rgba(255, 255, 255, 0.23) 20%), linear-gradient(-74deg, transparent 83%, rgba(255, 255, 255, 0.18) 15%), linear-gradient(-74deg, transparent 76%, rgba(255, 255, 255, 0.1) 15%), linear-gradient(to top, #127ab1, #1799e0, #1796db);
    		background-size: cover;
    		margin-bottom: 0px!important;
        }
        div{
            font-family: 'Geomanist', sans-serif;
  			font-weight: normal;
  			color: white;
            width: 85%;
            margin: 0 auto;
            position: relative;
            margin-top: 180px;
            transform: translateY(-50%);
        }
        h1{
            font-size: 50pt
        }
        h2{
            font-size: 28pt
        }
        </style>
    </head>

    <body>

       <img src=https://www.docker.com/sites/all/themes/docker/assets/images/brand-full.svg width=20%></img>

       <div class="inner">
            <h1>Welcome to the Docker Windows Workshop</h1>

            <h2>Congratulations on deploying your Windows Docker container!</h2>

		  	<p>

            </div>

    </body>

</html>
```

 - Create a script called "start.ps1" to be used as entrypoint and redirect the logs to the console:
```
Write-Output 'Starting w3svc'
Start-Service W3SVC
    
Write-Output 'Making HTTP GET call'
Invoke-WebRequest http://localhost -UseBasicParsing | Out-Null

Write-Output 'Flushing log file'
netsh http flush logbuffer | Out-Null

Write-Output 'Tailing log file'
Get-Content -path 'c:\iislog\W3SVC\u_extend1.log' -Tail 1 -Wait
```

- Create a "Dockerfile" with the content below:
```
FROM mcr.microsoft.com/windows/servercore:ltsc2019
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

#install iis
RUN powershell -Command \
    Add-WindowsFeature Web-Server; \
    Invoke-WebRequest -UseBasicParsing -Uri "https://dotnetbinaries.blob.core.windows.net/servicemonitor/2.0.1.6/ServiceMonitor.exe" -OutFile "C:\ServiceMonitor.exe"

RUN Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -filter 'system.applicationHost/log' -name 'centralLogFileMode' -value 'CentralW3C'; \
    Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -filter 'system.applicationHost/log/centralW3CLogFile' -name 'truncateSize' -value 4294967295; \
    Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -filter 'system.applicationHost/log/centralW3CLogFile' -name 'period' -value 'MaxSize'; \
    Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -filter 'system.applicationHost/log/centralW3CLogFile' -name 'directory' -value 'c:\iislog'

WORKDIR "C:\\"
COPY start.ps1 .
ENTRYPOINT ["powershell", "C:\\start.ps1"]

HEALTHCHECK --interval=2s \
 CMD powershell -command \
    try { \
     $response = Invoke-WebRequest http://localhost -UseBasicParsing; \
     if ($response.StatusCode -eq 200) { return 0} \
     else {return 1}; \
    } catch { return 1 }

COPY index.html "C:\\inetpub\\wwwroot"
```

 - Build the image using the command:
```
$ docker build -t web-app:1.0 .
```

 - Ensure the image was created:
```
$ docker images
```

 - Run the created image in interactive mode to attach to the container process:
```
$ docker run -it --name web-app -p 8080:80 web-app:1.0
```

 - Browse to the port 8080 to check if you can reach the application:
```
http://server-ip:8080
```

 - Exit from the container using:
```
$ CTRL + C
```

 - Cleanup:
```
$ docker rm -f web-app
```
 